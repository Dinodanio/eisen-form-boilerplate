import { render, screen } from '@testing-library/react';
import TextButton from './TextButton';
import userEvent from '@testing-library/user-event';

describe('Tests Text Button', () => {
  test('should render', () => {
    const { container } = render(
      <TextButton text={'Batman'} type={'button'} />
    );
    expect(container).toBeDefined();
  });

  test('should click', () => {
    const clickMock = jest.fn();
    render(<TextButton text={'Batman'} type={'button'} onClick={clickMock} />);

    // @ts-ignore
    userEvent.click(screen.queryByText(/Batman/));

    expect(clickMock).toHaveBeenCalledTimes(1);
  });
});
