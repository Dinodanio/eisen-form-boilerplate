import { ReactElement } from 'react';
import { Button } from 'primereact/button';

interface TextButtonProps {
  text: string;
  type: 'submit' | 'reset' | 'button' | undefined;
  onClick?: () => void;
}

const TextButton = ({ text, onClick, type }: TextButtonProps): ReactElement => {
  return (
    <Button onClick={onClick} type={type}>
      {text}
    </Button>
  );
};
export default TextButton;
