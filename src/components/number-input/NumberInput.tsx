import { ReactElement } from 'react';
import { Field, FieldProps } from 'formik';
import { InputNumber } from 'primereact/inputnumber';

interface NumberInputProps<T> {
  fieldName: keyof T;
}

const NumberInput = <T,>({ fieldName }: NumberInputProps<T>): ReactElement => {
  return (
    <Field name={fieldName}>
      {({ field }: FieldProps) => <InputNumber {...field} />}
    </Field>
  );
};

export default NumberInput;
