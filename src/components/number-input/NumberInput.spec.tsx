import { render } from '@testing-library/react';
import NumberInput from './NumberInput';
import { Formik } from 'formik';

describe('Tests Number Input', () => {
  test('should render', () => {
    const { container } = render(
      <Formik initialValues={{ one: 1 }} onSubmit={jest.fn()}>
        <NumberInput fieldName={'one'} />
      </Formik>
    );
    expect(container).toBeDefined();
  });
});
