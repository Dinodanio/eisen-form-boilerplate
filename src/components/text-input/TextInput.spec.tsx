import { render } from '@testing-library/react';
import NumberInput from './TextInput';
import { Formik } from 'formik';

describe('Tests Text Input', () => {
  test('should render', () => {
    const { container } = render(
      <Formik initialValues={{ one: 'Batman' }} onSubmit={jest.fn()}>
        <NumberInput fieldName={'one'} />
      </Formik>
    );
    expect(container).toBeDefined();
  });
});
