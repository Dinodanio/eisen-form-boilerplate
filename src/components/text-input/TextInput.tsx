import { ReactElement } from 'react';
import { Field, FieldProps } from 'formik';
import { InputText } from 'primereact/inputtext';

interface TextInputProps<T> {
  fieldName: keyof T;
}

const TextInput = <T,>({ fieldName }: TextInputProps<T>): ReactElement => {
  return (
    <Field name={fieldName}>
      {({ field }: FieldProps) => <InputText {...field} />}
    </Field>
  );
};

export default TextInput;
