import { ReactElement } from 'react';
import { useField } from 'formik';

interface TextRepresentationProps<T> {
  fieldName: keyof T;
}

const TextRepresentation = <T,>({
  fieldName,
}: TextRepresentationProps<T>): ReactElement => {
  const [field] = useField<string>({ name: fieldName.toString() });
  return (
    <div>
      <p>{field.value}</p>
    </div>
  );
};

export default TextRepresentation;
