import React from 'react';
import MainView from './main-view/MainView';

function App() {
  return (
    <>
      <MainView />
    </>
  );
}

export default App;
