import { ReactElement } from 'react';
import { Form, Formik } from 'formik';
import { FormikHelpers } from 'formik/dist/types';
import TextButton from '../components/text-button/TextButton';
import TextInput from '../components/text-input/TextInput';
import TextRepresentation from '../components/text-representation/TextRepresentation';
import NumberInput from '../components/number-input/NumberInput';

interface MyData {
  one: string;
  two: number;
}

const initialData: MyData = {
  one: 'one',
  two: 2,
};

const MainView = (): ReactElement => {
  const onSubmit = (values: MyData, formikHelpers: FormikHelpers<MyData>) => {
    console.log(values);
    console.log(formikHelpers);
  };

  return (
    <Formik initialValues={initialData} onSubmit={onSubmit}>
      <Form>
        <div>
          <TextInput<MyData> fieldName={'one'} />
          <TextRepresentation<MyData> fieldName={'one'} />
        </div>
        <div>
          <NumberInput<MyData> fieldName={'two'} />
          <TextRepresentation<MyData> fieldName={'two'} />
        </div>
        <TextButton text={'Submit'} type={'submit'} />
      </Form>
    </Formik>
  );
};

export default MainView;
