import {render} from "@testing-library/react";
import MainView from "./MainView";

describe('Tests Main View', ()=> {
    test('should render', () => {
        const {container} = render(<MainView/>)
        expect(container).toBeDefined();
    })
})